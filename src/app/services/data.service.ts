import { Injectable } from '@angular/core';
// import { retry } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class dataService {
    baseURL = '';
    wallet = '';
    constructor(private http: HttpClient) {
        this.baseURL = 'https://uat-backend.whrrl.in/';
        this.wallet = "https://uat-warehouses.whrrl.in/api/mswc/";
        // this.baseURL = 'http://localhost:4002/';
        // this.wallet = "http://localhost:4002/api/mswc/";
    }
    nftData() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(this.baseURL + 'nft/nftData', { headers: headers });
    }
    sendEth(to, value, int) {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(this.wallet + `eth-send/${to}/${value}/${int}`, { headers: headers });
    }
    balance(to) {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(this.wallet + `eth-balance/${to}/`, { headers: headers });
    }
    sendEthSave(data) {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.post(this.wallet + 'eth-send-save', data, { headers: headers });
    }
    ethUsdprice() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(this.wallet + 'band-price', { headers: headers });
    }
    getXagPrice() {
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return this.http.get(this.wallet + 'xag-price', { headers: headers });
    }
}
